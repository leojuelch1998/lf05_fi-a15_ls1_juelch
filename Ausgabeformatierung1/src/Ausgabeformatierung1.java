
public class Ausgabeformatierung1 {

	//Aufgabe 1
	
	public static void main(String[] args) {
			System.out.println("Das ist ein Beispielsatz");
			System.out.println("Ein \"Beispielsatz\" ist das");
	
	int alter = 23;
	String name = "Leonardo";
	
	System.out.print("Ich heiße " + name + " und bin " + alter + " Jahre alt\r");
	
	/*
	 print = In der aktuellen Zeile wird weiter geschrieben
	 println = In der nächsten Zeile wird weiter geschrieben
	 */
	
	//Aufgabe 2
	
	String s = "**************";
	
	System.out.printf("\r%8.2s", s);
	System.out.printf("\r%9.4s", s);
	System.out.printf("\r%10.6s", s);
	System.out.printf("\r%11.8s", s);
	System.out.printf("\r%12.10s", s);
	System.out.printf("\r%13.12s", s);
	System.out.printf("\r%8s", s);
	System.out.printf("\r%8.2s", s);
	System.out.printf("\r%8.2s", s);
	
	//Aufgabe 3
	
	double zahl1 = 22.4234234;
	double zahl2 = 111.2222;
	double zahl3 = 4.0;
	double zahl4 = 1000000.551;
	double zahl5 = 97.34;
	
	System.out.printf("\r\r%.2f", zahl1);
	System.out.printf("\n%.2f", zahl2);
	System.out.printf("\n%.2f", zahl3);
	System.out.printf("\n%.2f", zahl4);
	System.out.printf("\n%.2f", zahl5);
	}
}
