import java.util.Scanner;

public class Rabattsystem {
    public static void main(String[] args){
        while (true) {
            Scanner scanner = new Scanner (System.in);
            var wert = scanner.nextInt();

            if (wert <= 100) {
                System.out.println("Die Kosten sind " + wert * .9 + "€");
            } else if (wert > 100 && wert < 500) {
                System.out.println("Die Kosten sind " + wert * .85 + "€");
            } else {
                System.out.println("Die Kosten sind " + wert * .80 + "€");
            }

        }
    }
}
