import java.util.Scanner;

public class Taschenrechner {
    public static void main(String[] args) {
        while (true) {

            Scanner scanner = new Scanner(System.in);

            System.out.print("Gib die erste Zahl ein: ");
            double f = scanner.nextDouble();

            System.out.print("Gib die zweite Zahl ein: ");
            double g = scanner.nextDouble();

            try {
                while (true) {
                    System.out.print("Waehle zwischen + - * /: ");
                    String h = scanner.next();


                    switch (h) {
                        case "+" -> throw new TaschenrechnerResult(f + g);
                        case "-" -> throw new TaschenrechnerResult(f - g);
                        case "*" -> throw new TaschenrechnerResult(f * g);
                        case "/" -> throw new TaschenrechnerResult(f / g);
                        default -> {
                            System.out.println("Bitte nur oben erwaehnte Zeichen eingeben");
                        }
                    }
                }
            } catch (TaschenrechnerResult result) {
                System.out.println("das Ergebnis lautet " + result.getResult());
            }
        }
    }
}
