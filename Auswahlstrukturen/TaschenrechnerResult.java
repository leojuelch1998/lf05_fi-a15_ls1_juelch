public class TaschenrechnerResult extends Throwable {
    private final double result;

    public TaschenrechnerResult(double result) {
        this.result = result;
    }

    public double getResult() {
        return result;
    }
}
