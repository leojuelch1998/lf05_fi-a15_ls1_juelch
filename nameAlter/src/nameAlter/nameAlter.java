package nameAlter;

import java.util.Scanner;

public class nameAlter {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Wie ist dein Name?: ");
		String name = scanner.next();
		
		System.out.print("Wie alt bist du?: ");
		byte age = scanner.nextByte();
		
		System.out.println(name + " ist " + age + " alt.");
	}
}
