import java.util.Objects;
import java.util.Scanner;

public class JavaTrain {

    public static void main(String[] args) {

        System.out.print("\nHerzlich Wilkommen!\n\n");

        Scanner scanner = new Scanner(System.in);
        int fahrzeit = 8;

        System.out.print("Halt in Spandau? (y/n): ");
        if (Objects.equals(scanner.next(), "y")) {
            fahrzeit += 2;
        }

        System.out.print("Richtung Hamburg fahren? (y/n): ");
        if (Objects.equals(scanner.next(), "y")) {
            fahrzeit += 96;
            System.out.print("Sie erreichen Hamburg nach " + fahrzeit + " Minuten.");
        } else {
            fahrzeit += 34;

            System.out.print("Halt in Stendal? (y/n): ");
            fahrzeit += Objects.equals(scanner.next(), "y") ? 16 : 6;

            System.out.print("Bis Wolfsburg fahren? (y/n): ");
            if (Objects.equals(scanner.next(), "y")) {
                fahrzeit += 29;
                System.out.print("Sie erreichen Wolfsburg nach " + fahrzeit + " Minuten.");
            } else {

                System.out.print("Bis Braunschweig fahren? (y/n): ");
                if (Objects.equals(scanner.next(), "y")) {
                    fahrzeit += 50;
                    System.out.print("Sie erreichen Braunschweig nach " + fahrzeit + " Minuten.");
                } else {
                    fahrzeit += 62;
                    System.out.print("Sie erreichen Hannover nach " + fahrzeit + " Minuten.");
                }
            }
        }

    }

}