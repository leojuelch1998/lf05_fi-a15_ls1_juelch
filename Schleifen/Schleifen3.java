import java.util.Scanner;

public class Schleifen3 {
    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String[] c = String.valueOf(scanner.nextInt()).split("");
            int a = 0;
            for (String ch : c) {
                a += Integer.parseInt(ch);
            }

            System.out.println(a);

        }

    }
}
