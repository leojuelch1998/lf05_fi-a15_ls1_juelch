import java.util.Scanner;
public class Schleifen {
        public static void main(String[] args) {
            while (true) {
                try {
                    Scanner scanner = new Scanner(System.in);
                    int c, n = scanner.nextInt();

                    if (n < 1) {
                        throw new Exception("Nur natuerliche Zahlen");
                    }

                    for (int i = 1; i <= n; System.out.print(i++ + "\s")) ;
                    System.out.println();
                    for (int i = n; i > 0; System.out.print(i-- + "\s")) ;
                    System.out.println();
                    c = 0;

                    for (int i = 1; i <= n; c += i++) ;
                    System.out.println(c);
                    c = 0;
                    for (int i = 1; i <= n; c += 2 * i++) ;
                    System.out.println(c);
                    for (int i = 1; i <= n; c += 2 * i++ + 1) ;
                    System.out.println(c);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
    }
}
