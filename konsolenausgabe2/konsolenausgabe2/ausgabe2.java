package konsolenausgabe2;

public class ausgabe2 {

	public static void main(String[] args) {
		
	// AUFGABE 1		
	
		String s = "***********************";
		
		System.out.printf("\n%10.2s", s);
		System.out.printf("\n%5.1s", s);
		System.out.printf("%9.1s", s);
		System.out.printf("\n%5.1s", s);
		System.out.printf("%9.1s", s);
		System.out.printf("\n%10.2s", s);
		
	System.out.println("\r\r==================\r\r");
		
	// AUFGABE 2
	
		System.out.printf("%-5s = %-18s = %4d\n", "0!", "", 1);
	    System.out.printf("%-5s = %-18s = %4d\n", "1!", "1", 1);
	    System.out.printf("%-5s = %-18s = %4d\n", "2!", "1 * 2", 2);
	    System.out.printf("%-5s = %-18s = %4d\n", "3!", "1 * 2 * 3", 2 * 3);
	    System.out.printf("%-5s = %-18s = %4d\n", "4!", "1 * 2 * 3 * 4", 2 * 3 * 4);
	    System.out.printf("%-5s = %-18s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5", 2 * 3 * 4 * 5);
	    
	System.out.println("\r\r==================\r\r");

	// AUFGABE 3	
	
		String ab = "Fahrenheit";
	    String bc = "Celsius";
	    int a = 20;
	    double b = 28.8889;
	    int c = 10;
	    double d = 23.3333;
	    int e = 0;
	    double f = 17.7778;
	    int g = 20;
	    double h = 6.6667;
	    int i = 30;
	    double j = 1.1111;
	
	    System.out.printf("%-12s| %10s\n", ab,bc);
	    System.out.println("------------------------");
	    System.out.printf( "%-12d| %10.2f\n" , -a, -b);
	    System.out.printf( "%-12d| %10.2f\n" , -c, -d);
	    System.out.printf( "%-12d| %10.2f\n" , e, -f);
	    System.out.printf( "%-12d| %10.2f\n" , g, -h);
	    System.out.printf( "%-12d| %10.2f\n" , i, -j);

	}

}
